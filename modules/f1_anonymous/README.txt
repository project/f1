
There are times a site may way to display information from the F1 API to users
who are not logged in through the F1 API. This module provides the access through
the use of a user who can login for the site. The information here is used when
the user does not already have their own auth credentials from F1.

This should be used sparingly. But, there are times it is useful.

Installation
------------
See http://drupal.org/node/70151 for installation details.