<?php

/**
 * @file
 * The settings for the F1 Anonymous module.
 */

/**
 * Form callback; the settings form to authenticate a user.
 */
function f1_anonymous_settings_form() {
  $form = array();
  $form['descriptions'] = array(
    '#value' => '<p>'. t('Enter a username and password for a user inside F1 that can be used to consume the API for users who do not already have F1 API access.') .'</p>',
  );

  $form['f1_anonymous_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('f1_anonymous_username', NULL),
  );

  $form['f1_anonymous_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => variable_get('f1_anonymous_password', NULL),
  );

  $form['#validate'][] = 'f1_anonymous_settings_form_validate';

  return system_settings_form($form);
}

function f1_anonymous_settings_form_validate($form, &$form_state) {
  $options = array(
    'name' => $form_state['values']['f1_anonymous_username'],
    'pass' => $form_state['values']['f1_anonymous_password'],
  );

  $success = f1_anonymous_auth($options);

  if (!$success) {
    form_set_error('f1_anonymous_username', t('The username and password supplied were not able to authenticate against F1.'));
  }
}
