<?php

/**
 * @file
 * Admin settings forms for the F1 module.
 */

/**
 * Form callback; The admin settings form for the F1 module.
 */
function f1_api_settings_form() {
  $form = array();

  // Rest API settings.
  $form['rest_api'] = array(
    '#title' => t('REST API'),
    '#type' => 'fieldset',
    '#description' => t('2nd Party RESTful API Settings'),
    '#collapsed' => FALSE,
  );
  $form['rest_api']['f1_church_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Church Code'),
    '#description' => t('Enter the Church Code assigned by FellowshipOne.'),
    '#default_value' => variable_get('f1_church_code', NULL),
    '#size' => 50,
  );
  $form['rest_api']['f1_consumer_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Consumer Key'),
    '#description' => t('Enter the Consumer Key assigned by FellowshipOne.'),
    '#default_value' => variable_get('f1_consumer_key', NULL),
    '#size' => 50,
  );
  $form['rest_api']['f1_consumer_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Consumer Secret'),
    '#description' => t('Enter the Consumer Secret assigned by FellowshipOne.'),
    '#default_value' => variable_get('f1_consumer_secret', NULL),
    '#size' => 50,
  );
  $form['rest_api']['f1_sandbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sandbox'),
    '#description' => t('Should API calls contact a sandbox/staging site?'),
    '#default_value' => variable_get('f1_sandbox', FALSE),
  );
  $form['rest_api']['f1_remove_pass_request'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove Request new password links and form'),
    '#description' => t('This is userful if users are not able to change their password on the site.'),
    '#default_value' => variable_get('f1_remove_pass_request', FALSE),
  );
  $form['rest_api']['f1_user_login_type'] = array(
    '#type' => 'select',
    '#title' => t('User Authorization'),
    '#options' => array(
      'portal' => t('Portal'),
      'weblink' => t('Weblink'),
    ),
    '#default_value' => variable_get('f1_user_login_type', 'portal'),
  );

  $option_list = f1_auth_info_list();
  $options = array();
  if (!empty($option_list)) {
    foreach ($option_list as $k => $v) {
      $options[$k] = $v['description'];
    }
  }

  $form['rest_api']['f1_auth_type'] = array(
    '#type' => 'radios',
    '#title' => t('Authentication type'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => variable_get('f1_auth_type', 'f1_mixed'),
  );

  $form['rest_api']['f1_auth_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fellowship One Authentication Enabled'),
    '#default_value' => variable_get('f1_auth_enabled', TRUE),
  );
  // @todo Add validation to make sure the supplied info works.

  $form['#submit'][] = 'f1_api_settings_form_submit';

  return system_settings_form($form);
}

/**
 * When the remove pass request is selected the menu needs to be rebuilt.
 */
function f1_api_settings_form_submit($form, &$form_state) {
  menu_rebuild();
}