<?php

/**
 * @file
 * The controller for interacting with the token storage.
 */

interface f1TokenInterface {
  /**
   * Load the oauth tokens for a given user.
   *
   * @param int $uid
   *   The uid for a Drupal user.
   *
   * @return stdClass
   *   An object with the uid, token, and secret properties.
   */
  public function load($uid);

  /**
   * Save the oauth tokens for a given user.
   *
   * @param stdClass $record
   *   A record with following proerties:
   *   - uid: The user uid associated with the tokens.
   *   - token: The oauth token for the user.
   *   - secret: The oauth secret token for the user.
   *   - controller: The controller for object CRUD.
   */
  public function save($record);

  /**
   * Delere the oauth tokens stored for a given user.
   *
   * @param int $uid
   *   The uid for a Drupal user.
   */
  public function delete($uid);
}

class f1TokenControllerDefault implements f1TokenInterface {
  /**
   * Cache for records so they are not loaded more than once.
   */
  protected $cache = array();

  /**
   * @see f1TokenInerface::load()
   */
  public function load($uid) {

    // Load the record from cache if it is available.
    if (isset($this->cache[$uid])) {
      return $this->cache[$uid];
    }

    $result = db_query('SELECT * FROM {f1_tokens} WHERE uid = %d', $uid);
    $record = db_fetch_object($result);
    if ($record) {
      $record->controller = $this;

      // Cache the record for possible future access.
      $this->cache[$uid] = $record;
    }
    return $record;
  }

  /**
   * @see f1TokenInerface::save()
   */
  public function save($record) {
    // We need to see if we are adding a record or deleting one.
    $exists = $this->load($record->uid);
    unset($record->controller);
    if ($exists) {
      // The record exists so we update.
      drupal_write_record('f1_tokens', $record, array('uid'));
    }
    else {
      // A new record.
      drupal_write_record('f1_tokens', $record);
    }

    // Remove the old record from the cache.
    if (isset($this->cache[$record->uid])) {
      unset($this->cache[$record->uid]);
    }
  }

  /**
   * @see f1TokenInerface::delete()
   */
  public function delete($uid) {
    db_query('DELETE FROM {f1_tokens} WHERE uid = %d', $uid);
  }
}
