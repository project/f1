<?php

/**
 * @file
 * A base for different authentication schemes.
 */

/**
 * Provide a base (abstract) class for other F1 Auth classes to use.
 */
abstract class F1AuthBase {

  /**
   * The info defined in hook_f1_auth_info().
   *
   * @var $info
   */
  protected $info = array();

  /**
   * The token returned from F1 for a user.
   */
  protected $token = NULL;

  /**
   * The oauth token secret returned from F1 for a user.
   */
  protected $secret = NULL;

  /**
   * The response headers from token request.
   */
  protected $headers = array();

  /**
   * The username of an authenticated user.
   */
  protected $username = NULL;

  public function __construct($info) {
    $this->info = $info;
  }

  public function isEnabled() {
    $consumer = f1_consumer(array('use_tokens' => FALSE));
    return (variable_get('f1_auth_enabled', TRUE) && $consumer);
  }

  /**
   * Check if UID 1. UID 1 is a special case in Drupal and is the root user.
   *
   * @param string $name
   *   The username attempting to login.
   *
   * @return
   *   TRUE if the username is UID 1 and FALSE otherwise.
   */
  public function isUid1($name = '') {
    // If the user is UID 1 process against Drupal. This needs to happen so the
    // root account is always able to login.
    $result = db_query("SELECT uid FROM {users} WHERE name = '%s' AND uid = '1'", $name);
    if ($account = db_fetch_object($result)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Authenticate a user against F1.
   *
   * @param string $username
   *   The username to authenticate against.
   * @param string $password
   *   The corresponding password.
   *
   * @return
   *  TRUE if the user authenticated and FALSE if it failed.
   */
  public function authenticate($username, $password) {
    $return = FALSE;

    if (!empty($username)) {
      // The code here is taken from the example in f1-php index.php.
      $apiConsumer = f1_consumer(array('use_tokens' => FALSE));

      $auth_urls = array(
        'portal' => '/v1/PortalUser/AccessToken',
        'weblink' => '/v1/WeblinkUser/AccessToken',
      );
      $auth_selection = variable_get('f1_user_login_type', 'portal');

      $requestURL =  sprintf( "%s%s", $apiConsumer->getBaseUrl(), $auth_urls[$auth_selection]);
      $requestBody = Util::urlencode_rfc3986(base64_encode( sprintf( "%s %s", $username, $password)));

      // This is important. If we dont set this, the post will be sent using Content-Type: application/x-www-form-urlencoded (curl will do this automatically)
      // Per OAuth specification, if the Content-Type is application/x-www-form-urlencoded, then all the post parameters also need to be part of the base signature string
      // To override this, we need to set Content-type to something other than application/x-www-form-urlencoded
      $getContentType = array("Accept: application/json",  "Content-type: application/json");
      $requestBody = $apiConsumer->postRequest($requestURL, $requestBody , $getContentType,  200);
      preg_match( "~oauth_token\=([^\&]+)\&oauth_token_secret\=([^\&]+)~i", $requestBody, $tokens );
      if (isset($tokens[1]) && isset($tokens[2])) {
        $this->token = $tokens[1];
        $this->secret = $tokens[2];
        $this->headers = $apiConsumer->getResponseHeader();
        $this->username = $username;
        $return = TRUE;
      }
    }

    return $return;
  }

  /**
   * Create a new user associated with F1.
   *
   * @return stdClass
   *   A user object (already having been saved to the database).
   */
  public function createUser() {
    // Generate a random drupal password. F1 will be used for auth anyways.
    $pass_new = user_password(20);

    // The path to the user account is supplied in the response when we asked
    // for a token. Here we get the path.
    foreach ($this->headers as $val) {
      $start = 'Content-Location:';
      $contentLocation = substr($val, 0, 17);
      if ($contentLocation == $start) {
        $personLocation = str_replace($start, "", $val);
        continue;
      }
    }

    // To get an email address we need to look up a user and then do a search to
    // include their communications which could include an email address. Yeah,
    // that's a lot of work to get an email address. Glad we don't have to do
    // that often.
    $consumer = f1_consumer(array('token' => $this->token, 'secret' => $this->secret));
    $getContentType = array("Accept: application/json",  "Content-type: application/json");
    $response = $consumer->doRequest($personLocation, $getContentType);
    if ($response == '') {
      return FALSE;
    }
    $response = json_decode(strstr($response, '{"person":{'), TRUE);
    $id = $response['person']['@id']; // The id for the user to use in search.

    $request_url = $consumer->getBaseUrl() . AppConfig::$f1_people_search .'?id='. $id .'&include=communications';
    $response = $consumer->doRequest($request_url, $getContentType);
    if ($response == '') {
      return FALSE;
    }
    $response = json_decode(strstr($response, '{"results":{'), TRUE);

    // Cycle through the communications search results looking for the first
    // email address for a user. Since we searched by id there will only be one
    // result in the search.
    $communications = $response['results']['person'][0]['communications']['communication'];
    $email = NULL;
    foreach ($communications as $k => $v) {
      if ($v['communicationType']['name'] == 'Email') {
        $email = $v['communicationValue'];
        continue;
      }
    }

    // If mail attribute is missing, set the name as mail.
    // @todo Lookup an actual email address and add it.
    $mail = ($email) ? $email : $this->username;

    $userinfo = array('name' => $this->username, 'pass' => $pass_new, 'mail' => $mail, 'init' => $mail, 'status' => 1, 'f1_person' => $personLocation);
    $account = user_save(NULL, $userinfo);
    watchdog('F1', 'New F1 user %name was created.', array('%name' => $this->username), WATCHDOG_NOTICE, l(t('edit'), 'user/'. $account->uid .'/edit'));

    return $account;
  }

  /**
   * Update the tokens stored in Drupal to the ones supplied at login.
   *
   * @param int $uid
   *   The user id for the account to update.
   */
  public function saveTokens($uid) {
    $record = new stdClass();
    $record->uid = $uid;
    $record->token = $this->token;
    $record->secret = $this->secret;

    $controller = _f1_token_controller();
    $controller->save($record);
  }

  abstract public function validate(array $values = array());
}

/**
 * Interface for the F1 Auth Option.
 */
interface F1AuthInterface {

  /**
   * When the class is created by f1_auth_controller() the info defined in
   * hook_f1_auth_info() for this option is passed into the constructor.
   *
   * @param array $info
   *   Information for the option described in hook_f1_auth_info().
   */
  public function __construct($info);

  /**
   * If the F1 Auth for this setup is properly configured and enabled. This is
   * useful if there is setup for this functionality that needs to be checked.
   *
   * @return bool
   *   TRUE if enable and FALSE if not.
   */
  public function isEnabled();

  /**
   * Authenticate a user.
   *
   * @param $values
   *   The authentication values passed in from different possible forms. For
   *   more detail see user_authenticate().
   */
  public function validate(array $values = array());
}