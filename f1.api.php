<?php

/**
 * @file
 * The hooks provided by the f1 module.
 */

/**
 * Alter the options used when building the API consumer.
 *
 * For more detail see f1_consumer().
 *
 * @param array $options
 */
function hook_f1_client_options_alter(&$options) {

}

/**
 * Alter the access priveledges for F1.
 *
 * @param bool $access
 *   Whether the user has access.
 * @param int $uid
 *   The uid for a given user.
 * @param $record
 *   The record containing the token and token secret or FALSE if none provided.
 */
function hook_f1_access_alter(&$access, $uid, $record) {

}

/**
 * Add authentication metods to the list of available options.
 *
 * The F1 module provides the ability to have a pluggable authentication scheme
 * with different rules. The two examples provided by the F1 modules are for
 * mixed and exclusive authentication. That is drupal site users mixed in with
 * F1 user and for users exclusively from F1.
 *
 * @return array
 *   An array of information specifying the details about an authentication
 *   scheme. The key is the machine name for an option and the value is an array
 *   of details being:
 *   - descriptions: The description as seen in the interface.
 *   - class: The class that implements F1AuthInterface for the scheme.
 *   - cache: If the object can be cached or not.
 *
 *   For an example see f1_f1_auth_info().
 */
function hook_f1_auth_info() {

}

/**
 * Alter the options provided by hook_f1_auth_info().
 */
function f1_auth_info_alter($options) {

}