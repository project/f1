
The FellowshipOne (F1) module provides a pluggable framework to alter and extend
the module as well as helper functions making integration with the F1 API easier.

-----------------------
Working with the F1 API
-----------------------
There are two main functions to use when utilizing the F1 API. f1_access()
provides an access callback to see if a given user has access to the F1 API.
When no arguments are passed in the current logged in user is used. When a user
id is passed in that user is checked.

The f1_consumer() builds an api consumer object that can be used to consume
the F1 API. If not enough detail is available to build the consumer object FALSE
is returned. For more details on the API see http://developer.fellowshipone.com/
and the library provided by F1 at http://github.com/fellowshiptech/f1api-php.

For an example using these functions see the f1_example module.

------------------------------------
Altering and Extending the F1 Module
------------------------------------
The F1 module provides four hooks to register and alter how it works as well as
three pluggable systems. These are useful if you want to extend or alter the way
the module work and not for using the module to consume the API.

The hooks are outlined in f1.api.php.

The pluggable systems are:
- F1 Token Storage. The oauth token and oauth token secret are stored per user
  in the database. But, the storage location is pluggable. This happens through
  the use of a class implementing f1TokenInterface and the class name is set
  via the Drupal variable f1_token_controller.
- The oauth consumer class. The consumer class provided by default is OAuthClient.
  This is included in the f1api-php library from github. This class can be
  changed globally with the Drupal variable f1_class or when f2_consumer() is
  called by passing in a different class with the class option. If the class is
  altered from OAuthClient a class suppling the same interface should be used.
- The authentication scheme. For more detail on this see hook_f1_auto_info() in
  f1.api.php. This provides a mechanism to change how authentication happens
  when users login from F1.
