
F1 is a family of modules providing integration with the FellowShip One church
management system. 

At the core of the module is the F1 module that provides integration with the
service. Additional modules can be enabled which provide integration with
features from FellowShip One.

Installation
------------

1. Download the f1api Library from http://github.com/fellowshiptech/f1api-php.
   Place the uncompressed files in either /sites/all/libraries, /sites/[domain name]/libraries,
   or in the libraries folder within your install profile.